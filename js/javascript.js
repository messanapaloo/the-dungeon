// Initialisation des emojis du joueur
let personnage = '😎'
let personnageMalade = '🤢';
let personnageRich = '🤑';
let dollar = '💲';
let tete = '☠';
let coffre = '💰';

let phone = document.querySelectorAll('.phone');
//Initialisation de la variable qui determine s'il faut mettre un dollar ou un un piege
let generateur;

//Initialisation du compteur de sac de d0llor
let compteurPiece = 0;

//Initialiusation du compteur de sac d'or
let compteurSac = 0;

// Initilisqtiond du no,bre de coffret par partie
let coffret = Math.floor(Math.random()*9+3 );

//Initiqlisqtion de lq position du joueur au debut du jeu
let random = Math.floor(Math.random()*30+68 );

// Initialisation de SCORE à 0
let score = 0;

// Initialisation de SAUT  à 0
let saut = 0;

// Initialisation de la variable de la fonction acheter coffret
let acheter;

//Recuperation de la variable sauts
let sautId = document.getElementById("sauts");

//Recuperation de la variable niveau
let nivId = document.getElementById("niv");

//Initialisation de la vie à 30
let vie = 30;

// Initialisation du l'image pour recommencer 
let reset;

//Recuperation de la variable de vie dans le html
let vieId = document.querySelectorAll('.vie');

//Recuperation de la variable scrore
let scoreId = document.getElementById("scores");

//Recuperation du tableau
let grid = document.querySelectorAll ('.column');

// Initalisation des variables des fonctions des donjoncs
let donjon1 = document.getElementById("donjon1");
let donjon2 = document.getElementById("donjon2");
let donjon3 = document.getElementById("donjon3");

//Initialisation de la variable du choix de  donjon aleatoirement 
let aleatoire = document.getElementById("aleatoire");

//Initialisation de la variable start pour la fonction start plus bas
let start;

//Initialisation de la variable qui permet de savoir a quel niveau le joueur joue
let passage = 1;

// Recuperation du div qui contient
let direction = document.querySelector('.Direction');

// Initialisation de la variable qui identifie les donjons
let donjon; 

//Recuperation des textes qui se trouve dans la fenetre
let textes = document.querySelectorAll('.textes');

// Recuperaiton de l'image restart
let restart = document.querySelector('.exit');

//Cache l'image restart au debut du jeu
restart.style.visibility = "hidden";

// Recuperation de la fenetre
let modal = document.getElementById("myModal");

// Recuperation du Span qui contient la crois
let span = document.getElementsByClassName("close")[0];

// La fentre s'affiche au demarrage du jeu
modal.style.display = "block";

// Recuperation du content de la fenetre
let content = document.getElementById('content');

// La fenetre se ferme quand l'utilisateur clique sur la croit
span.onclick = function() {
  modal.style.display = "none";
}

// Si l'utilisateur clique n'importe quelle emdroit dans la page la fentre se ferme.
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}


for ( let a = 0; a <vieId.length; a++ ){
        vieId[a].style.backgroundColor = "#008080";
    }

/**
 * Cette fonction est le point de depart Du jeu elle met en place certain emoji et offre la premiere interface au jouer
 */
    start = function (){
        for ( let i = 0; i < grid.length; i++){
           generateur =Math.floor(Math.random()*101+1 );
           grid[i].style.backgroundColor ='#e8f1e4e7';
             if ( generateur >=1 & generateur <=75){
            grid[i].innerText = "☠";}
            else {
                grid[i].innerText = "💲"
            }
        }

        for ( let e = 0; e <= coffret; e++){
            grid[Math.floor(Math.random()*grid.length+1 )].innerHTML = coffre;
        }
    } 

    /**
     * Cette fonction fait recommencer le jeu quand le joueur clique sur l'image restart
     */
reset = function (){
    location.reload();
}

/**
 * Initialisation du premier donjon moins complexe
 */
donjon1 = function(){
    for( let o = 0; o < phone.length; o++){
         phone[o].style.display = "table-cell";
    }
    donjon=1;
    nivId.innerText = passage;
    saut += 0;
    sautId.innerText = saut;
    start();
    grid[random].innerText = personnage ;
    grid[grid.length-1].innerText = '➬';
    grid[0].innerText = "";
    grid[0].style.backgroundColor ="#72132b";
    grid[12].innerText = "";
    grid[12].style.backgroundColor ="#72132b";
    grid[130].innerText = "";
    grid[130].style.backgroundColor ="#72132b";
    grid[141].innerText = "";
    grid[141].style.backgroundColor ="#72132b";
    grid[128].innerText = "";
    grid[128].style.backgroundColor ="#72132b";
    grid[129].innerText = "";
    grid[129].style.backgroundColor ="#72132b";
}

/**
 * Initialisation du deuxieme donjon moyennement complexe
 */
donjon2 = function(){
    for( let o = 0; o < phone.length; o++){
        phone[o].style.display = "table-cell";
   }
    donjon=2;
    nivId.innerText = passage;
    saut += 1;
    sautId.innerText = saut;
    start();
    random = 84;
    grid[random].innerText = personnage ;
   
    grid[grid.length-1].innerText = '➬';
    grid[29].innerHTML = "";
    grid[29].style.backgroundColor ="#72132b";
    grid[43].innerText = "";
    grid[43].style.backgroundColor ="#72132b";
    grid[35].innerText = "";
    grid[35].style.backgroundColor ="#72132b";
    grid[44].innerText = "";
    grid[44].style.backgroundColor ="#72132b";
    grid[45].innerText = "";
    grid[45].style.backgroundColor ="#72132b";
    grid[46].innerText = "";
    grid[46].style.backgroundColor ="#72132b";
    grid[47].innerText = "";
    grid[47].style.backgroundColor ="#72132b";
    grid[58].innerText = "";
    grid[58].style.backgroundColor ="#72132b";
    grid[70].innerText = "";
    grid[70].style.backgroundColor ="#72132b";
    grid[71].innerText = "";
    grid[71].style.backgroundColor ="#72132b";
    grid[72].innerText = "";
    grid[72].style.backgroundColor ="#72132b";
    grid[83].innerText = "";
    grid[83].style.backgroundColor ="#72132b";
    grid[85].innerText = "";
    grid[85].style.backgroundColor ="#72132b";
    grid[95].innerText = "";
    grid[95].style.backgroundColor ="#72132b";
    grid[107].innerText = "";
    grid[107].style.backgroundColor ="#72132b";
    grid[97].innerText = "";
    grid[97].style.backgroundColor ="#72132b";
    grid[99].innerText = "";
    grid[99].style.backgroundColor ="#72132b";
    grid[113].innerText = "";
    grid[113].style.backgroundColor ="#72132b";
    grid[56].innerText = "";
    grid[56].style.backgroundColor ="#72132b";
    grid[60].innerText = "";
    grid[60].style.backgroundColor ="#72132b";
    grid[0].innerText = "";
    grid[0].style.backgroundColor ="#72132b";
    grid[12].innerText = "";
    grid[12].style.backgroundColor ="#72132b";
    grid[130].innerText = "";
    grid[130].style.backgroundColor ="#72132b";
    grid[141].innerText = "";
    grid[141].style.backgroundColor ="#72132b";
    grid[128].innerText = "";
    grid[128].style.backgroundColor ="#72132b";
    grid[129].innerText = "";
    grid[129].style.backgroundColor ="#72132b";
   

}

/**
 * Initialisation du premier donjon plus complexe
 */
donjon3 = function(){
    for( let o = 0; o < phone.length; o++){
        phone[o].style.display = "table-cell";
   }
    donjon = 3;
    nivId.innerText = passage;
    saut += 1;
    sautId.innerText = saut;
    start();
    random = 72;
    grid[random].innerText = personnage ; 
    grid[grid.length-1].innerText = '➬';
    grid[0].innerText = "";
    grid[0].style.backgroundColor ="#72132b";
    grid[1].innerText = "";
    grid[1].style.backgroundColor ="#72132b";
    grid[13].innerText = "";
    grid[13].style.backgroundColor ="#72132b";
    grid[14].innerText = "";
    grid[14].style.backgroundColor ="#72132b";
    grid[11].innerText = "";
    grid[11].style.backgroundColor ="#72132b";
    grid[12].innerText = "";
    grid[12].style.backgroundColor ="#72132b";
    grid[24].innerText = "";
    grid[24].style.backgroundColor ="#72132b";
    grid[25].innerText = "";
    grid[25].style.backgroundColor ="#72132b";
    grid[117].innerText = "";
    grid[117].style.backgroundColor ="#72132b";
    grid[118].innerText = "";
    grid[118].style.backgroundColor ="#72132b";
    grid[130].innerText = "";
    grid[130].style.backgroundColor ="#72132b";
    grid[131].innerText = "";
    grid[131].style.backgroundColor ="#72132b";
    grid[141].innerText = "";
    grid[141].style.backgroundColor ="#72132b";
    grid[128].innerText = "";
    grid[128].style.backgroundColor ="#72132b";
    grid[129].innerText = "";
    grid[129].style.backgroundColor ="#72132b";
    grid[47].innerText = "";
    grid[47].style.backgroundColor ="#72132b";
    grid[46].innerText = "";
    grid[46].style.backgroundColor ="#72132b";
    grid[45].innerText = "";
    grid[45].style.backgroundColor ="#72132b";
    grid[44].innerText = "";
    grid[44].style.backgroundColor ="#72132b";
    grid[43].innerText = "";
    grid[43].style.backgroundColor ="#72132b";
    grid[56].innerText = "";
    grid[56].style.backgroundColor ="#72132b";
    grid[57].innerText = "";
    grid[57].style.backgroundColor ="#72132b";
    grid[58].innerText = "";
    grid[58].style.backgroundColor ="#72132b";
    grid[59].innerText = "";
    grid[59].style.backgroundColor ="#72132b";
    grid[60].innerText = "";
    grid[60].style.backgroundColor ="#72132b";
    grid[69].innerText = "";
    grid[69].style.backgroundColor ="#72132b";
    grid[70].innerText = "";
    grid[70].style.backgroundColor ="#72132b";
    grid[71].innerText = "";
    grid[71].style.backgroundColor ="#72132b";
    grid[73].innerText = "";
    grid[73].style.backgroundColor ="#72132b";
    grid[82].innerText = "";
    grid[82].style.backgroundColor ="#72132b";
    grid[83].innerText = "";
    grid[83].style.backgroundColor ="#72132b";
    grid[84].innerText = "";
    grid[84].style.backgroundColor ="#72132b";
    grid[85].innerText = "";
    grid[85].style.backgroundColor ="#72132b";
    grid[86].innerText = "";
    grid[86].style.backgroundColor ="#72132b";
    grid[95].innerText = "";
    grid[95].style.backgroundColor ="#72132b";
    grid[96].innerText = "";
    grid[96].style.backgroundColor ="#72132b";
    grid[97].innerText = "";
    grid[97].style.backgroundColor ="#72132b";
    grid[98].innerText = "";
    grid[98].style.backgroundColor ="#72132b";
    grid[99].innerText = "";
    grid[99].style.backgroundColor ="#72132b";

}

/**
 * Cette fonction choisit aleatoirement un donjon au joueur
 */
aleatoire = function(){
    let e = Math.floor(Math.random()*3 );
    for( let o = 0; o < phone.length; o++){
        phone[o].style.display = "table-cell";
   }

    if ( e ===0){
        donjon =1;
        donjon1();
        
    }
    else if ( e === 1){
         donjon =2;
        donjon2();
       
    }
    else if ( e ===3) {
        donjon =3;
        donjon3();
       
    }
    else{
        donjon =3;
        donjon3();
    }
    passage = 1;

}

/**
 * Permet d'acheter un coffret qui peut contenir un sac d'or, un point  de vie ou encore saut a condition que le joueur ait assez d'argent
 */
acheter = function (){
    if (score > 375){
        if ( Math.floor(Math.random()*3 ) == 0){
        score -= 375;
        scoreId.innerText = score;
        saut+=1;
        sautId.innerText =saut;
        }
        else if(Math.floor(Math.random()*3 )== 1 ) {
            if ( vie+3 <= 30){
                            vie +=3;
                            controle();
                        }
                        else {
                            vie = 30
                            controle();
                        }
            score -= 375;
            scoreId.innerText = score;
        }
        else {
            score += 500;
            scoreId.innerText = score;
        }
    }
    else {
        content.style.height = "40%";
        for ( let i = 0; i < textes.length; i++){
            textes[i].innerText="";}
            textes[3].innerText= '😔😔😔  Oups pas assez de sous  😔😔😔';
            modal.style.display = "block";
        }
    
}

/**
 * Cette fonction gere les humeur du joueur en foction de ce qui se trouve sur son chemin
 */
 function couleurEmoji (){
    if (grid [random].innerText ===dollar){
        personnage = personnageRich;
        score +=50; 
        scoreId.innerText = score;
        compteurPiece += 1;
        
    }
    else if ( grid[random].innerText === tete){
        personnage = personnageMalade;
        score -=10;
        scoreId.innerText = score;
        if ( vie-1.5 >=0){
            vie -=1.5;
            controle();
        }
        else {
            vie = 0
        }
    }
    else if ( grid[random].innerText === coffre){
        personnage = "🥳";
        score +=300;
        scoreId.innerText = score;
        compteurSac += 1;
    }
    else{
        personnage = "😎";
    }
}

/**
 * Si le joueur essai de passer un niveau sans repondre aux conditions ou que sa vi est finie le jeu s'arrete et cette focntioin affiche le message adequat
 */
let fin = function(){

    content.style.height = "55%";
    for ( let i = 0; i < textes.length; i++){
        textes[i].innerText="";}
        textes[3].innerText= '😔😔😔  Oups ta partie est finie  😔😔😔';
        textes[4].innerText = 'Ton Score est de : ' +  "♻ " + score + " ♻";
        textes[5].innerText = 'Clique sur reset pour recommencer!!👇!!';
        modal.style.display = "block";
        restart.style.visibility = "visible";
        direction.style.visibility = "hidden";
    }

 /**
  * Cette fonction tres essentielle fait la transition entre les niveaux
  * Elle verifie si la vie est supererieur à zero, ensuite si le jour est positionné à l'avant dernier position.
  * Si c'est le cas elle verifie si le joueur a ramasser au moins 3 sac d'or et 10 pieces.
  * Puis En fonction du passage un donjon sera charger
  * S'il a fait le trois passage le jeu se termine et lui affiche son score
  */
function NivSup (){
    if ( vie <= 30 & vie >0){
        if (grid[random] ===grid[grid.length-1] & passage ===1){
        
            if( compteurSac >= 3 && compteurPiece >= 10){
                passage +=1;
                compteurPiece = 0;
                compteurSac = 0;
                if ( donjon ===1){
    
                    donjon2 ();
                    random = 84;
                    grid[random].innerText = personnage ;
                    grid[grid.length-1].innerText = '➬';   
                }
                else if ( donjon === 2){
                    donjon3();
                    random = 72;
                    grid[random].innerText = personnage ;
                    grid[grid.length-1].innerText = '➬';
                }
                else if (donjon ===3) {
    
                    donjon1();
                    random = Math.floor(Math.random()*30+68 );
                    grid[random].innerText = personnage ;
                    grid[grid.length-1].innerText = '➬';
                }
                
            }
            else {
                fin();
            }
        }
        else if (grid[random] ===grid[grid.length-1] & passage === 2 ) {
            
            if( compteurSac >= 3 && compteurPiece >= 10){
                    passage +=1;
                    compteurPiece = 0;
                    compteurSac = 0;
                    if ( donjon === 1){
                        donjon2 ();
                        random = 84;
                        grid[random].innerText = personnage ;
                        grid[grid.length-1].innerHTML= '<img src="./img/tenor.gif" alt="Victoire">';
                    }
                    else if ( donjon === 2){
                        donjon3();
                        random = 72;
                        grid[random].innerText = personnage ;
                        grid[grid.length-1].innerHTML= '<img src="./img/tenor.gif" alt="Victoire">';                 
                    }
                    else if (donjon === 3) {
                        donjon1();
                        random = Math.floor(Math.random()*30+68 );
                        grid[random].innerText = personnage ;
                        grid[grid.length-1].innerHTML= '<img src="./img/tenor.gif" alt="Victoire">';
                    }  
                }
                else {
                 fin();
                }
            }
            else if (grid[random] ===grid[grid.length-1] & passage ===3 ){
                fin();
                textes[3].innerText= '🎉🎉🎉🎉🎉  Bravo tu as passé tous les Donjon  🎉🎉🎉';                
            }
        }
        else {
            fin();
            textes[3].innerText= "🤭🤭🤭hummmmm !!! Tu n'as plus assez de Vie ";
        }
    }
    

/**
 * Cette fonction permet de faire bouger le joueur vers le haut
 */
let clickHaut = function (){

    if ( vie <= 30 & vie >0 ){
        if( random-13 >= 0 & grid[random-13].innerText !=="" ){
            grid [random].innerText =".";
            grid[random].style.borderBottom = 'none';
            random -=13;
            couleurEmoji();
            grid [random].innerText =personnage;
            grid[random].style.borderBottom = '0.2em solid #72132b';
            }
    }
    else {
        fin();
        textes[3].innerText= "🤭🤭🤭hummmmm !!! Tu n'as plus assez de Vie ";
        }
    }

/**
 * Cette fonction permet de faire bouger le joueur vers le bas
 */
let clickBas = function(){
    if ( vie <= 30 & vie >0){
        if( random+13 <= grid.length-1 & grid[random+13].innerText !=="" ){
        grid [random].innerText =".";
        grid[random].style.borderBottom = 'none';
        random +=13;
        couleurEmoji();
        grid [random].innerText =personnage;
        grid[random].style.borderBottom = '0.2em solid #72132b';
        }
    }
    else{
        fin();
        textes[3].innerText= "🤭🤭🤭hummmmm !!! Tu n'as plus assez de Vie ";
    }
}

/**
 * Cette fonction fait bouger le joueur vers la gauche
 */
let clickGauche = function(){
    if ( vie <= 30 & vie >0){
        if( random-1 >=0 & random-1<= grid.length-1 & grid[random-1].innerText !=="" ){
        grid [random].innerText =".";
        grid[random].style.borderBottom = 'none';
        random -=1;
        couleurEmoji();
        grid [random].innerText =personnage;
        grid[random].style.borderBottom = '0.2em solid #72132b';
        }
    }
    else {
        fin();
        textes[3].innerText= "🤭🤭🤭hummmmm !!! Tu n'as plus assez de Vie ";
    }
}

/**
 * Cette fonction permet de faire bouger le joueur vers la droite
 */
let clickDroite = function(){
    if( vie <= 30 & vie >0 ){
        if( random+1 >=0 & random+1<= grid.length-1 & grid[random+1].innerText !==""){
        grid [random].innerText =".";
        grid[random].style.borderBottom = 'none';
        random +=1;
        couleurEmoji();
        grid [random].innerText =personnage;
        grid[random].style.borderBottom = '0.2em solid #72132b';
        }
    }
    else{
        fin();
        textes[3].innerText= "🤭🤭🤭hummmmm !!! Tu n'as plus assez de Vie ";
    }
}

/**
 * Cette fonction effectye le saut du joueur
 */
let clickSaut = function(){
    if ( vie <= 30 & vie >0 ){
        if( random+2 >=0 & random+2<= grid.length-1 & saut >0  ){
            
        grid [random].innerText =".";
        grid[random].style.borderBottom = 'none';

        if  ( grid[random] === grid[116]){
            random =142;
        }
        else {
            random +=2;
        }
        saut -=1;
        sautId.innerText = saut;
        couleurEmoji();
        NivSup();
        grid [random].innerText =personnage;
        grid[random].style.borderBottom = '0.2em solid #72132b';
        }
        else{
            content.style.height = "40%";
            for ( let i = 0; i < textes.length; i++){
                textes[i].innerText="";}
                textes[3].innerText= '😔!❌! Pas assez de sauts  veuillez en acheter !❌!😔';
                modal.style.display = "block";
        }
    }
    else {
        fin();
        textes[3].innerText= "🤭🤭🤭hummmmm !!! Tu n'as plus assez de Vie !❌! ";
    }
}

/**
 * Cette fonction controle l'evolution  de la vie 
 * Si le joueur marche sur un piege sa vie diminue de 1.5 point la voyant vire au jaune.
 * A cahque perte 3 point de vie le voyant passe au rouge
 * Si par chance le joueur trouve dans un coffret une barre de vie la bare suivante passe au bleu.
 */

let controle = function(){
    switch ( vie ){
        case 30 : 
        for ( let i = 0; i < vieId.length; i++ ){
            vieId[i].style.backgroundColor = "#008080";
        }
        break;
        case 28.5 : 
        for ( let i = 0; i <9; i++ ){
            vieId[i].style.backgroundColor = "#008080";
        } 
            vieId[9].style.backgroundColor= "yellow";
        break;
        case 27: 
        for ( let i = 0; i <9; i++ ){
            vieId[i].style.backgroundColor = "#008080";
        } 
            vieId[9].style.backgroundColor= "red";
        break;
        case 25.5 :
            for ( let i = 0; i <8; i++ ){
                vieId[i].style.backgroundColor = "#008080";
            } 
            for ( a =9; a >7 ;a--){
                vieId[a].style.backgroundColor= "red";
            }
            vieId[8].style.backgroundColor= "yellow";
        break;
        case 24:
            for ( let i = 0; i <8; i++ ){
                vieId[i].style.backgroundColor = "#008080";
            } 
            for ( a =9; a >7 ;a--){
                vieId[a].style.backgroundColor= "red";
            }
        break;
        case 22.5 :
            for ( let i = 0; i <7; i++ ){
                vieId[i].style.backgroundColor = "#008080";
            } 
            for ( a =9; a >6 ;a--){
                vieId[a].style.backgroundColor= "red";
            }
            vieId[7].style.backgroundColor= "yellow";
        break;
        case 21:
            for ( let i = 0; i <7; i++ ){
                vieId[i].style.backgroundColor = "#008080";
            } 
            for ( a =9; a >6 ;a--){
                vieId[a].style.backgroundColor= "red";
            }
        break;
        case 19.5 : 
        for ( let i = 0; i <6; i++ ){
            vieId[i].style.backgroundColor = "#008080";
        } 
        for ( a =9; a >5 ;a--){
            vieId[a].style.backgroundColor= "red";
        }
        vieId[6].style.backgroundColor= "yellow";
        break;
        case 18: 
        for ( let i = 0; i <6; i++ ){
            vieId[i].style.backgroundColor = "#008080";
        } 
        for ( a =9; a >5 ;a--){
            vieId[a].style.backgroundColor= "red";
        }
        break;
        case 16.5 :
            for ( let i = 0; i <5; i++ ){
                vieId[i].style.backgroundColor = "#008080";
            } 
            for ( a =9; a >4 ;a--){
                vieId[a].style.backgroundColor= "red";
            }
            vieId[5].style.backgroundColor = "yellow";
        break;
        case 15:
            for ( let i = 0; i <=4; i++ ){
                vieId[i].style.backgroundColor = "#008080";
            } 
            for ( a =9; a >=5 ;a--){
                vieId[a].style.backgroundColor= "red";
            }
        break;
        case 13.5 : 
        for ( let i = 0; i <4; i++ ){
            vieId[i].style.backgroundColor = "#008080";
        } 
        for ( a =9; a >3 ;a--){
            vieId[a].style.backgroundColor= "red";
        }
        vieId[4].style.backgroundColor = "yellow";
    break;
        case 12:
            for ( let i = 0; i <=3; i++ ){
                vieId[i].style.backgroundColor = "#008080";
            } 
            for ( a =9; a >=4 ;a--){
                vieId[a].style.backgroundColor= "red";
            }
        break;
        case 10.5 : 
        for ( let i = 0; i <3; i++ ){
            vieId[i].style.backgroundColor = "#008080";
        } 
        for ( a =9; a >2 ;a--){
            vieId[a].style.backgroundColor= "red";
        }
        vieId[3].style.backgroundColor = "yellow";
    break;
        case 9:
            for ( let i = 0; i <=2; i++ ){
                vieId[i].style.backgroundColor = "#008080";
            } 
            for ( a =9; a >=3 ;a--){
                vieId[a].style.backgroundColor= "red";
            }
        break;
        case 7.5 :
            for ( let i = 0; i <2; i++ ){
                vieId[i].style.backgroundColor = "#008080";
            } 
            for ( a =9; a >3 ;a--){
                vieId[a].style.backgroundColor= "red";
            }
            vieId[2].style.backgroundColor = "yellow";
        break;

        case 6 :
            for ( let i = 0; i <=1; i++ ){
                vieId[i].style.backgroundColor = "#008080";
            } 
            for ( a =9; a >=2 ;a--){
                vieId[a].style.backgroundColor= "red";
            }
        break;
        case 4.5 :
            for ( let i = 0; i <1; i++ ){
                vieId[i].style.backgroundColor = "#008080";
            } 
            for ( a =9; a >0 ;a--){
                vieId[a].style.backgroundColor= "red";
            }
            vieId[1].style.backgroundColor = "yellow";
        break;
        case 3:
            for ( let i = 0; i <1; i++ ){
                vieId[i].style.backgroundColor = "#008080";
            } 
            for ( a =9; a >=1 ;a--){
                vieId[a].style.backgroundColor= "red";
            }
        break;
        case 1.5: 
        for ( a =9; a >=0 ;a--){
            vieId[a].style.backgroundColor= "red";
        }
        vieId[0].style.backgroundColor = "yellow";
    break;
        case 0:    
            for ( a =9; a >=0 ;a--){
                vieId[a].style.backgroundColor= "red";
            }
        break;
    }
}